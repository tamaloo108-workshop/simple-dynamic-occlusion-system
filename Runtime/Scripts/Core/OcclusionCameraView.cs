﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;


namespace SDOS.Core
{
    public class OcclusionCameraView : MonoBehaviour
    {
        public bool Active => settings.Active;
        public Transform TargetPlayer;
        public LayerMask Layers;

        public float viewRadius = 320;
        [Range(0, 360)]
        public float viewAngle = 130;

        public delegate void CameraViewRender(Collider col);
        public delegate void CameraViewNotRender();

        public static event CameraViewRender OnCameraViewRender;
        public static event CameraViewNotRender OnCameraViewNotRender;

        DynamicOcclusionSettings settings;

        Collider[] cols;

        Transform _tempTarget;
        Vector3 _tempDirToTarget;
        float _tempCurAngle;

        private void Awake()
        {
            #region Setup
            settings = SDOSCore.LoadFromResources<DynamicOcclusionSettings>(SDOSCore.DYNAMIC_SETTING_PATH, "DynamicSettings");
            #endregion

        }

        private void Start()
        {
            //use invoke repeating for preserve frame rate and reduce unnecessary calls (imho).
            InvokeRepeating("RenderUpdate", 1f, 0.02f);
        }

        void RenderUpdate()
        {
            if (Active)
            {
                //only active when not On Trigger Occlusion
                if (settings.PreferedOcclusionType == OcclusionType.OnTriggerOcclusion) return;
                OnCameraViewNotRender.Invoke();
                FindVisible();
            }
        }

        public Vector3 DirFromAngle(float v1, bool v2)
        {
            if (!v2)
            {
                v1 += TargetPlayer.eulerAngles.y;
            }
            return new Vector3(Mathf.Sin(v1 * Mathf.Deg2Rad), 0, Mathf.Cos(v1 * Mathf.Deg2Rad));
        }

        private void FindVisible()
        {
            cols = Physics.OverlapSphere(TargetPlayer.position, viewRadius, Layers);
            for (int i = 0; i < cols.Length; i++)
            {
                _tempTarget = cols[i].transform;
                _tempDirToTarget = (_tempTarget.position - TargetPlayer.position).normalized;
                _tempCurAngle = Vector3.Angle(TargetPlayer.forward, _tempDirToTarget);
                if (_tempCurAngle < viewAngle / 2)
                {

                    OnCameraViewRender.Invoke(cols[i]);
                }
            }
        }

    }
}