﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace SDOS.Core
{
    public class SDOSCore
    {
        public static void CreateColliderOccludee()
        {
            GameObject p = new GameObject();
            BoxCollider coll = p.AddComponent<BoxCollider>();
            p.gameObject.name = "Occlusion Collider";
            coll.isTrigger = true;
            coll.gameObject.AddComponent<OcclusionAreaHandler>();
        }

        public static void CreateCameraView()
        {
            GameObject p = new GameObject();
            p.AddComponent<OcclusionCameraView>();
            p.gameObject.name = "Camera Render View";
        }


        public static void GetMembers(Collider myCollider, out HashSet<GameObject> members)
        {
            members = new HashSet<GameObject>();
            var _members = Physics.OverlapBox(myCollider.bounds.center, myCollider.bounds.size / 2, Quaternion.identity);

            for (int i = _members.Length - 1; i >= 0; i--)
            {
                if (_members[i] == myCollider) continue;

                if (_members[i].GetComponent<OcclusionAreaHandler>() != null) continue;

                if (_members[i].TryGetComponent(out ITriggerOcclusion t))
                    continue;

                if (!members.Contains(_members[i].transform.gameObject))
                    members.Add(_members[i].transform.gameObject);
            }
        }


        public static T LoadFromResources<T>(string folderName, string name) where T : UnityEngine.Object
        {
            var obj = Resources.Load<T>(Path.Combine(SETTING_DEF_FILE_PATH, folderName, name));
            return obj;
        }

        #region Asset Database
        public const string ROOT_ASSET_FOLDER = "Assets";
        public const string RESOURCES_FOLDER_PATH = "Resources";
        public const string SETTING_DEF_FILE_PATH = "Data";
        public const string DYNAMIC_SETTING_PATH = "DynamicSettings";

#if UNITY_EDITOR

        public async static void CreateSettingsFile<T>(T types, string path, string name) where T : UnityEngine.Object
        {
            string resDir = Path.Combine(ROOT_ASSET_FOLDER, RESOURCES_FOLDER_PATH);
            string settingDir = Path.Combine(ROOT_ASSET_FOLDER, RESOURCES_FOLDER_PATH, SETTING_DEF_FILE_PATH);
            string DynamicSettingDir = Path.Combine(ROOT_ASSET_FOLDER, RESOURCES_FOLDER_PATH, SETTING_DEF_FILE_PATH, path);

            //check resource folder.
            if (AssetDatabase.IsValidFolder(resDir))
            {
                //check data folder.
                if (AssetDatabase.IsValidFolder(settingDir))
                {
                    //check dynamic setting folder.
                    if (AssetDatabase.IsValidFolder(DynamicSettingDir))
                    {
                        //create setting file now.
                        CreateFiles(types, path, name);
                    }
                    else
                    {
                        //create dynamic setting folder first.
                        await Task.Delay(10);
                        AssetDatabase.CreateFolder(settingDir, path);
                        CreateFiles(types, path, name);
                    }
                }
                else
                {
                    //create data folder first.
                    AssetDatabase.CreateFolder(resDir, SETTING_DEF_FILE_PATH);
                    await Task.Delay(10);
                    AssetDatabase.CreateFolder(settingDir, path);
                    await Task.Delay(10);
                    CreateFiles(types, path, name);
                }
            }
            else
            {
                //create resources folder first.
                AssetDatabase.CreateFolder(ROOT_ASSET_FOLDER, RESOURCES_FOLDER_PATH);
                await Task.Delay(10);
                AssetDatabase.CreateFolder(resDir, SETTING_DEF_FILE_PATH);
                await Task.Delay(10);
                AssetDatabase.CreateFolder(settingDir, path);
                await Task.Delay(10);
                CreateFiles(types, path, name);
            }
        }


        static void CreateFiles<T>(T types, string path, string name) where T : UnityEngine.Object
        {
            string dir = Path.Combine(ROOT_ASSET_FOLDER, RESOURCES_FOLDER_PATH, SETTING_DEF_FILE_PATH, path, name);
            AssetDatabase.CreateAsset(types, dir + ".asset");
            AssetDatabase.SaveAssets();
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = types;
        }
#endif

        #endregion
    }

}
