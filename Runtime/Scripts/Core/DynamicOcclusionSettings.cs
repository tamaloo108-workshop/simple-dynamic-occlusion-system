﻿using UnityEngine;


namespace SDOS.Core
{
    public class DynamicOcclusionSettings : ScriptableObject
    {
        public OcclusionType PreferedOcclusionType;
        public bool EnableOnStart = true;
        public bool Active = true;
    }


    [System.Serializable]
    public enum OcclusionType
    {
        OnTriggerOcclusion = 0,
        BlockRenderCameraView = 1,
        SingleRenderCameraView = 2
    }
}

