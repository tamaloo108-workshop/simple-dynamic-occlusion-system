﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SDOS.Core
{
    public class OcclusionAreaHandler : MonoBehaviour
    {
        private Collider myCollider;
        private HashSet<GameObject> OccludeeGroup;
        private Dictionary<GameObject, List<Renderer>> OccludeeGroupDict = new Dictionary<GameObject, List<Renderer>>();
        private DynamicOcclusionSettings setting;
        private bool EnableMemberOnStart;

        public bool Active => setting.Active;
        private bool AreaActive = false;

        private void Awake()
        {
            #region Setup
            setting = SDOSCore.LoadFromResources<DynamicOcclusionSettings>(SDOSCore.DYNAMIC_SETTING_PATH, "DynamicSettings");
            myCollider = GetComponent<Collider>();
            EnableMemberOnStart = setting.EnableOnStart;
            #endregion
        }

        // Start is called before the first frame update
        void Start()
        {
            SDOSCore.GetMembers(myCollider, out OccludeeGroup);
            EnableMember(OccludeeGroup, EnableMemberOnStart);

            foreach (var item in OccludeeGroup)
            {
                //we know one of the child does not have renderer.
                if (!OccludeeGroupDict.ContainsKey(item.gameObject))
                {
                    //cache it into dictionary.
                    OccludeeGroupDict.Add(item.gameObject, new List<Renderer>());
                    for (int i = 0; i < item.transform.parent.childCount; i++)
                    {
                        if (item.transform.parent.GetChild(i).GetComponent<Renderer>() != null)
                        {
                            OccludeeGroupDict[item.gameObject].Add(item.transform.parent.GetChild(i).GetComponent<Renderer>());
                        }
                    }
                }
            }
        }

        private void OnEnable()
        {
            if (setting.PreferedOcclusionType != OcclusionType.OnTriggerOcclusion)
            {
                OcclusionCameraView.OnCameraViewRender += OcclusionCameraView_OnCameraViewRender;
                OcclusionCameraView.OnCameraViewNotRender += OcclusionCameraView_OnCameraViewNotRender;
            }
        }

        private void OnDisable()
        {
            if (setting.PreferedOcclusionType != OcclusionType.OnTriggerOcclusion)
            {
                OcclusionCameraView.OnCameraViewRender -= OcclusionCameraView_OnCameraViewRender;
                OcclusionCameraView.OnCameraViewNotRender -= OcclusionCameraView_OnCameraViewNotRender;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!Active) return;
            if (setting.PreferedOcclusionType != OcclusionType.OnTriggerOcclusion) return;

            if (other.TryGetComponent(out ITriggerOcclusion t))
            {
                EnableMember(OccludeeGroup, true);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (!Active) return;
            if (setting.PreferedOcclusionType != OcclusionType.OnTriggerOcclusion) return;

            if (other.TryGetComponent(out ITriggerOcclusion t))
            {
                EnableMember(OccludeeGroup, false);
            }
        }

        internal void EnableMember(GameObject member, bool enabled)
        {
            //let make it gc-friendly.
            if (OccludeeGroupDict.ContainsKey(member))
            {
                for (int i = 0; i < OccludeeGroupDict[member].Count; i++)
                {
                    OccludeeGroupDict[member][i].enabled = enabled;
                }
            }
        }

        internal void EnableMember(HashSet<GameObject> members, bool enabled)
        {
            foreach (var member in members)
            {
                if (member.transform.parent != null)
                {
                    EnableMember(member, enabled);
                }
            }
        }

        internal void EnableMember(HashSet<GameObject> OccludeeGroup, bool enabled, Collider OcclusionMember, bool singleRender = false)
        {
            if (OccludeeGroup.Contains(OcclusionMember.gameObject))
            {
                if (singleRender)
                {
                    EnableMember(OcclusionMember.gameObject, enabled);
                    return;
                }

                if (!AreaActive)
                {
                    AreaActive = true;
                    EnableMember(OccludeeGroup, enabled);
                }
            }
        }

        private void OcclusionCameraView_OnCameraViewNotRender()
        {
            EnableMember(OccludeeGroup, false);
            AreaActive = false;
        }

        private void OcclusionCameraView_OnCameraViewRender(Collider col)
        {
            if (setting.PreferedOcclusionType == OcclusionType.BlockRenderCameraView)
                EnableMember(OccludeeGroup, true, col);
            else if (setting.PreferedOcclusionType == OcclusionType.SingleRenderCameraView)
                EnableMember(OccludeeGroup, true, col, true);
        }


    }
}