﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SDOS.Example
{
    public class MouseCamLook : MonoBehaviour
    {

        [SerializeField]
        public float speedH = 5.0f;
        [SerializeField]
        public float speedV = 2.0f;

        private float yaw = 0.0f;
        private float pitch = 0.0f;

        // Update is called once per frame
        void Update()
        {
            yaw += speedH * Input.GetAxis("Mouse X");
            pitch += speedV * Input.GetAxis("Mouse Y");

            transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
        }
    }
}