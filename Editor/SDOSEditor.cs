﻿using UnityEditor;
using SDOS.Core;
using UnityEngine;

namespace SDOS.Editor
{
#if UNITY_EDITOR
    public class SDOSEditor
    {
        [MenuItem("SDOS/Create Collider Detection", priority = 1)]
        static void DoGameObjectCollider()
        {
            SDOSCore.CreateColliderOccludee();
        }

        [MenuItem("SDOS/Create Dynamic Occlusion Settings", priority = 0)]
        static void CreateDynamicOcclusionSettings()
        {
            DynamicOcclusionSettings s = ScriptableObject.CreateInstance<DynamicOcclusionSettings>();
            SDOSCore.CreateSettingsFile(s, SDOSCore.DYNAMIC_SETTING_PATH, "DynamicSettings");
        }

        [MenuItem("SDOS/ Create Camera Render View", priority = 1)]
        static void SetupCameraView()
        {
            SDOSCore.CreateCameraView();
        }

    }
#endif
}