﻿using SDOS.Core;
using UnityEditor;
using UnityEngine;

namespace SDOS.Editor
{
    [CustomEditor(typeof(OcclusionCameraView))]
    public class FOVEditor : UnityEditor.Editor
    {
        void OnSceneGUI()
        {
            OcclusionCameraView fow = (OcclusionCameraView)target;
            Handles.color = Color.blue;
            Vector3 viewAngleA = fow.DirFromAngle(-fow.viewAngle / 2, false);
            Vector3 viewAngleB = fow.DirFromAngle(fow.viewAngle / 2, false);
            Handles.DrawWireArc(fow.TargetPlayer.position, Vector3.up, viewAngleA, fow.viewAngle, fow.viewRadius);

            Handles.DrawLine(fow.TargetPlayer.position, fow.TargetPlayer.position + viewAngleA * fow.viewRadius);
            Handles.DrawLine(fow.TargetPlayer.position, fow.TargetPlayer.position + viewAngleB * fow.viewRadius);

            Handles.color = Color.red;

        }
    }
}